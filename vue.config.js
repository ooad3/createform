module.exports = {
  publicPath: process.env.NODE_ENV === 'production'
    ? '/~cs62160320/learn_bootstrap/'
    : '/'
}
